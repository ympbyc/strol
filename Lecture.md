Connected Sensors Lecture
=========================

https://bitbucket.org/ympbyc/strol

Basic Course ~3hrs.

Let's use your smartphones sensors and connect them to the internet.

### Setup

#### Windows

Download This repository.

Install Chocolatey <small>by pasting the following command into cmd.exe</small>small>

```bat
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
```

Install Meteor <small>by typing the following command into cmd.exe</small>

```bat
choco install meteor
```




Learn about Meteor

https://www.meteor.com/

Learn about Smartphone Sensors and How to use it from JavaScript

https://mobiforge.com/design-development/sense-and-sensor-bility-access-mobile-device-sensors-with-javascript

#### Contact

Minori Yamashita (Nori) minori.yamashita@pilotz.jp