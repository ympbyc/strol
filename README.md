Strol
=====

init 2018-04

### Usage

+ install meteor
+ clone this repo
+ meteor run
+ goto localhost:3000

Create MyMap with Google map
https://drive.google.com/open?id=1QCGoRJmtlSuVPL2yJzfQESVyevODYjgD&usp=sharing

export KML and convert it to GeoJSON here
https://mygeodata.cloud/converter/kml-to-geojson