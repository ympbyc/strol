`import { Meteor } from 'meteor/meteor'`
`import { Mongo } from 'meteor/mongo'`
`import { HTTP } from 'meteor/http'`
`import { Template } from 'meteor/templating'`
`import { Session } from 'meteor/session'`


Epicenters = new Mongo.Collection "epicenters";


earth_radius = 6371


spherical_distance = ({latitude: lat1, longitude: lon1}, {latitude: lat2, longitude: lon2}) ->
        dLat  = (lat2 - lat1) * Math.PI / 180
        dLong = (lon2 - lon1) * Math.PI / 180
        a     = (Math.sin dLat / 2) * (Math.sin dLat / 2) +
                (Math.cos lat1 * Math.PI / 180) *
                (Math.cos lat2 * Math.PI / 180) *
                (Math.sin dLong / 2) *
                (Math.sin dLong / 2)
        c     = 2 * (Math.atan2 (Math.sqrt a), (Math.sqrt 1 - a))
        d     = earth_radius * c
        d

gyro_compass_heading = (alpha, beta, gamma) ->
                _x = if beta  then deg_to_rad(beta)  else 0
                _y = if gamma then deg_to_rad(gamma) else 0
                _z = if alpha then deg_to_rad(alpha) else 0
                cY = Math.cos _y
                cZ = Math.cos _z
                sX = Math.sin _x
                sY = Math.sin _y
                sZ = Math.sin _z
                Vx = -cZ * sY - sZ * sX * cY
                Vy = -sZ * sY + cZ * sX * cY
                compassHeading = Math.atan(Vx / Vy)
                if Vy < 0
                    compassHeading += Math.PI
                else if Vx < 0
                    compassHeading += 2 * Math.PI
                compassHeading * ( 180 / Math.PI )


items_within_distance_from = (coll, distance, coords) ->
        coll.fetch().filter ({reclat, reclong}) ->
                d = spherical_distance {latitude: reclat, longitude: reclong}, coords
                d < distance

areacodes =
        japan:      {latitude: 35.0, longitude: 135.0}
        california: {latitude: 38.5, longitude: -121.494399600000010000}


area_epicenters = _.memoize (areacode) ->
        items_within_distance_from (Epicenters.find {}), 1000, areacodes[areacode]

nearby_epicenters = (pos) ->
        items_within_distance_from (Epicenters.find {}), 50, pos


parse_geo_json = (data) ->
    data.features.map (d)->
        reclong: d.geometry.coordinates[0],
        reclat:  d.geometry.coordinates[1],
        name: d.properties.Name,
        descriptions: d.properties.description,
        point:   d.geometry.coordinates.join ","



if Meteor.isClient
        console.log "hello"
        Session.setDefault "close-epicenters", []
        Session.setDefault "heading", 0
        Session.setDefault "on_spot", false
        Session.setDefault "spot", {name: "nowhere"}

        bgClasss = _.shuffle ["bg-sunflower", "bg-orange", "bg-carrot",
                              "bg-pumpkin", "bg-alizarin",
                              "bg-emerald", "bg-nephritis", "bg-peterriver",
                              "bg-belizehole"]
        len = bgClasss.length

        close_epicenters = ->
                ecs = Session.get("close-epicenters")
                myLoc = Session.get("my-original-location")
                if !myLoc then return []
                _.map(_.groupBy(ecs, (ec)->ec.descriptions || "castle"), (v,k)->
                    cat: k, spots: v
                )
                #ecs = ecs.map (ec) ->
                #        {reclat: lat, reclong: lng} = ec
                #        ec.distance = spherical_distance {latitude: lat, longitude: lng}, myLoc
                #        ec
                #_.sortBy ecs,
                #         (ec)->ec.distance

        bgClass = (->
                idx = -1
                -> ++idx; bgClasss[idx % len])

        Template.akau.helpers
                title: "Castle Rader"
                stateTitle: ->
                        if Session.get("on_spot") then "YOU ARE ON" else "THINGS NEARBY"

        Template.spots.helpers
                epicenters: close_epicenters
                bgClass: bgClass()

        Template.descriptions.helpers
                epicenters: close_epicenters
                bgClass: bgClass()

        Template.spot.helpers
                show: -> Session.get("on_spot")
                spot: -> Session.get "spot"


        shrinkAllDesc = -> ($ ".spot-desc").removeClass "expanded"

        negateDisp = {"block": "hidden", "hidden": "block", "": "block"}
        shownCats = []

        Template.spots.events
                "click li": (e, tmpl) ->
                    e.target.querySelector(".spot-individual").classList.toggle("hidden")
                    cat = e.target.querySelector(".cat-name").innerText
                    if e.target.querySelector(".spot-individual").classList.contains("hidden")
                        shownCats = _.without shownCats cat
                    else
                        shownCats.push(cat)

                "click .spot-individual": (e, tmpl) ->
                    name = e.target.getAttribute "data-name"
                    $desc = ($ "#descriptions").find ".spot-desc[data-name=\"#{name}\"]"
                    shrinkAllDesc()
                    $desc.addClass "expanded"

        Template.akau.events
                "click #compass": shrinkAllDesc


        app = _.extend {}, Backbone.Events

        latlng = (lat, lng) -> new google.maps.LatLng lat, lng

        deg_to_rad = _.memoize (deg) -> deg * (Math.PI / 180)

        compute_heading = (ll1, ll2) ->
                google.maps.geometry.spherical.computeHeading ll1, ll2

        #dupricate to spherical_distance.  prefer this on the client side
        compute_distance = (ll1, ll2) ->
                google.maps.geometry.spherical.computeDistanceBetween ll1, ll2


        bonsai = window.bonsai

        bonsai_worker = ->
                compass_r = 140
                center = 160
                zoom = 0.03

                pos = (dist, heading) ->
                        tilt = Math.PI / 2
                        xpos: (dist * (Math.cos heading - tilt)) + center
                        ypos: (dist * (Math.sin heading - tilt)) + center


                colors = ["#8FD3FF", "#7ACBFF", "#57BBFB", "#52B6F6", "#3B9FDF", "#2B8FCF", "#1A7EBE"]
                radius = [1, 0.99, 0.97, 0.93, 0.85, 0.69, 0.37]
                circle = (x, y, radius, color) ->
                        (new Circle x, y, radius)
                                .fill(color)
                                .addTo stage
                circle center, center, compass_r * radius[x], colors[x] for x in [0...(Math.min radius.length, colors.length)]

                grp = (new Group).addTo stage
                stage.on "message:clear", -> grp.clear()
                stage.on "message:epicenter", ({distance, needleAngle, name}) ->
                        dist = distance * zoom
                        dist = if dist > compass_r then compass_r else dist
                        {xpos, ypos} = pos dist, needleAngle
                        (new Circle xpos, ypos, 9 - dist * 0.05)
                                .fill("#e74c3c")
                                .addTo grp
                        (new Text name)
                                .addTo(grp)
                                .attr
                                        x:        xpos
                                        y:        ypos
                                        fontSize: 12
                needle = (heading) ->
                        {xpos: x1, ypos: y1} = pos 160, heading
                        {xpos: x2, ypos: y2} = pos 142, heading - 0.05
                        {xpos: x3, ypos: y3} = pos 142, heading + 0.05
                        (new Path)
                                .moveTo(x1, y1)
                                .lineTo(x2, y2)
                                .lineTo(x3, y3)
                                .closePath()
                                .addTo grp

                stage.on "message:heading", (heading) ->
                        (needle heading).fill "#ecf0f1"
                        (needle heading - Math.PI).stroke "#ecf0f1", 1


        window.onload = ->
                bonsai = bonsai.run (document.getElementById "compass"), {
                        code:   bonsai_worker
                        width:  320
                        height: 320
                }


        draw_compass = ({name}, angle, distance) ->
                bonsai.sendMessage "epicenter",
                        needleAngle: deg_to_rad angle
                        distance:    distance
                        name:        name


        render_compass = (ecs, {latitude, longitude, heading}) ->
                you_pos = latlng latitude, longitude
                bonsai.sendMessage "clear", {}
                ecs.filter((ec)->shownCats.indexOf(ec.descriptions) > -1).forEach (ec) ->
                        {reclat, reclong} = ec
                        pos = latlng reclat, reclong
                        draw_compass ec,
                                     (compute_heading you_pos, pos) + heading,
                                     compute_distance you_pos, pos
                bonsai.sendMessage "heading", (deg_to_rad heading)

        test_nearness = (epicenters, pos) ->
                nears = _.filter epicenters, (epicenter)->
                        (compute_distance (latlng epicenter.reclat, epicenter.reclong), (latlng pos.latitude, pos.longitude)) < 5000
                unless _.isEmpty nears
                        app.trigger "spotted", nears[0]
                else
                        app.trigger "left_spot"

        app.on "spotted", (epicenter) ->
                Session.set "spot", epicenter
                Session.set "on_spot", yes


        app.on "left_spot", (epicenter) ->
                Session.set "on_spot", no


        watch_geolocation = (f) ->
                loc  = null
                head = 0
                window.navigator.geolocation.watchPosition ((pos) -> loc = _.pick(pos.coords, "latitude", "longitude")),
                                                   (err) -> alert err,
                                                   enableHighAccuracy: true,
                                                   maximumAge: 600
                window.addEventListener "deviceorientation", (e) ->
                        head = gyro_compass_heading(e.alpha, e.beta, e.gamma)
                        #f(_.assoc loc, "heading", head)

                #setInterval (-> 
                #    if loc 
                #        f(_.assoc loc, "heading", head)), 500

                anim = ->
                        requestAnimationFrame anim
                        if loc
                                f(_.assoc loc, "heading", head)
                requestAnimationFrame anim

        #mock geolocation source
        fake_geolocation = (f) ->
                fn = (lat, lon, hed) ->
                        f {latitude: lat, longitude: lon, heading: hed}
                        setTimeout (-> fn lat + 0.001, lon + 0.005, hed + 0.01), 1000
                setTimeout (-> fn 38.80, -121, 1), 1000


        user_pos = (user) ->
                {latitude, longitude} = pos = user.profile.location
                _.conj (_.omit pos, "latitude", "longitude"), reclat: latitude, reclong: longitude


        window.timestamp = Date.now() - 30001
        watch_geolocation (pos) ->
                #Session.set "my-location", pos
                if (Date.now() - window.timestamp > 30000)
                        if window.navigator.vibrate
                                window.navigator.vibrate(200)
                        window.timestamp = Date.now()
                        Session.set "my-original-location", pos
                        Session.set "close-epicenters", (nearby_epicenters pos)
                render_compass Session.get("close-epicenters"), pos
                test_nearness  Session.get("close-epicenters"), pos





if Meteor.isServer
        Meteor.startup ->
                console.log "Hello strol"
                

        Meteor.methods
                empty: -> Epicenters.remove {}
                populate: ->
                    #HTTP.get "https://strol.tk/castles.json",
                    #            (err, result) ->
                    #                    _.each result.data,
                    #                            (x) -> Epicenters.insert x
                    HTTP.get "http://strol.tk/kamiyama.json",
                                (err, result) ->
                                        _.each parse_geo_json(result.data),
                                               (x) -> Epicenters.insert x
                insert_epc: (data) ->
                    Epicenters.insert data
                remove_epc: (selector) ->
                    Epicenters.remove selector
                epicenters: nearby_epicenters
                area_epcs:  area_epicenters
